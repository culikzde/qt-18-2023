#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsPolygonItem>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

const int N = 4;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow * ui;
    QGraphicsScene * scene;
    QGraphicsPolygonItem * board [3][N][2*N];
    void addPawn (int z, int x, int y, QColor c);
    void addPoint (QPointF p);
    void draw (QPointF AB[N], QPointF CD[N]);
};

#endif // MAINWINDOW_H

