#include "game.h"
#include "ui_game.h"
#include <QGraphicsRectItem>
#include <QGraphicsEllipseItem>
#include <QGraphicsPolygonItem>
#include <QGraphicsTextItem>
#include <QApplication>
#include <cmath>
#include <iostream>
using namespace std;

/* ---------------------------------------------------------------------- */

const double sqrt3_2 = std::sqrt (3) / 2;

const double a = 80;

double sin30 (int n)
{
    switch (n % 12)
    {
        case 0: return 0; break;
        case 1: return 0.5; break;
        case 2: return sqrt3_2; break;
        case 3: return 1; break;
        case 4: return sqrt3_2; break;
        case 5: return 0.5; break;
        case 6: return 0; break;
        case 7: return - 0.5; break;
        case 8: return - sqrt3_2; break;
        case 9: return - 1; break;
        case 10: return - sqrt3_2; break;
        case 11: return - 0.5; break;
    }
    return 0;
}

inline double cos30 (int n)
{
    return sin30 (n+3);
}

QPointF rot30 (int x, int y, int n) /* n=1 ... 30 stupnu */
{
    return QPointF (cos30(n)*x + sin30(n)*y, -sin30(n)*x + cos30(n)*y);
}

QPointF rot30 (QPointF p, int n) /* n=1 ... 30 stupnu */
{
    return rot30 (p.x(), p.y(), n);
}

/* ---------------------------------------------------------------------- */

QString name (int x, int y, int z)
{
    return QString::number (z) + ", " + QString::number (y)+ "," + QString::number (x);
}

void MainWindow::addPawn (int x, int y, int z, QColor c)
{
    QPolygonF p = board [z][y][x]->polygon ();

    QPointF sum (0, 0);
    int cnt = p.count();
    for (int i = 0; i < cnt; i++)
        sum += p.at(i);
    sum /= cnt;

    QGraphicsEllipseItem * e = new QGraphicsEllipseItem;
    e->setPos (sum + QPointF (20, 20)); // !?
    e->setRect (-40, -40, 40, 40);
    e->setPen (QColor ("blue"));
    e->setBrush (c);
    e->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable);

    QGraphicsTextItem * t = new QGraphicsTextItem;
    t->setPlainText ("\u2659");

    QFont f = t->font ();
    f.setPointSize (f.pointSize() + 16);
    t->setFont (f);
    t->setPos (-40, -50);

    t->setParentItem (e);

    scene->addItem (e);
}

void MainWindow::addPoint (QPointF p)
{
    qreal x = p.x();
    qreal y = p.y();
    QGraphicsEllipseItem * e = new QGraphicsEllipseItem;
    e->setPos (x, y);
    e->setRect (-10, -10, 10, 10);
    e->setPen (QColor ("blue"));
    e->setBrush (QColor ("orange"));
    e->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable);
    scene->addItem (e);
}

void line (QPointF A, QPointF B, QPointF AB[N+1])
{
    for (int i = 0; i <= N; i++)
        AB[i] = A + (B-A) * i / N;
}

void MainWindow::draw (QPointF AB[N+1], QPointF CD[N+1])
{
    for (int i = 0; i <= N; i++)
        scene->addLine (AB[i].x(), AB[i].y(), CD[i].x(), CD[i].y(), QColor ("orange"));
}

QPointF intersection (QPointF A, QPointF B, QPointF C, QPointF D)
{
    // primka AB  p(t) = A+t*(B-A)
    // primka CD  q(u) = C+u*(D-C)
    QLineF AB (A, B);
    QLineF CD (C, D);
    QPointF result;
    QLineF::IntersectType answer = AB.intersects(CD, & result);
    assert (answer != QLineF::NoIntersection);
    return result;
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    resize (800, 800);

    scene = new QGraphicsScene;
    ui->graphicsView->setScene (scene);

    // scene->setSceneRect (-320, -320, 320, 320);
    // scene->setSceneRect (0, 0, 640, 640);

    // scene->addLine (-640, 0, 640, 0, QColor ("red"));
    // scene->addLine (0, -640, 0, 640, QColor ("red"));

    QPointF Z = QPoint (0, 0);
    QPointF A = rot30 (0, a*4, 0);
    QPointF B = rot30 (0, a*4/sqrt3_2, 1);
    QPointF C = rot30 (0, a*4, 2);

    QPointF ZA [N+1]; line (Z, A, ZA);
    QPointF AB [N+1]; line (A, B, AB);
    QPointF CB [N+1]; line (C, B, CB);
    QPointF ZC [N+1]; line (Z, C, ZC);

    QPointF V[N+1][N+1];
    for (int i = 0; i <= N; i++)
        for (int k = 0; k <= N; k++)
            V[i][k] = intersection (ZA[k], CB[k], AB[i], ZC[i]);

    for (int z = 0; z < 2; z++)
        for (int y = 0; y < N; y++)
            for (int x = 0; x < 2*N; x++)
                board [z][y][x] = nullptr;

    for (int t = 0; t < 6; t++)
    {
        QPointF W[N+1][N+1];
        for (int i = 0; i <= N; i++)
            for (int k = 0; k <= N; k++)
                W[i][k] = rot30 (V[i][k], 2*t);

        for (int i = 0; i < N; i++)
        {
            for (int k = 0; k < N ; k++)
            {
                QGraphicsPolygonItem * box = new QGraphicsPolygonItem;
                QPolygonF polygon;
                polygon << W [i]  [k];
                polygon << W [i+1][k];
                polygon << W [i+1][k+1];
                polygon << W [i]  [k+1];
                box->setPolygon (polygon);

                box->setPen (QColor ("blue"));
                if ((i+k+t)%2 == 1)
                   box->setBrush (QColor ("cornflowerblue"));
                else
                   box->setBrush (QColor ("yellow"));

                box->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable);
                scene->addItem (box);

                int z = ((t+1)/2) % 3;
                int x;
                if (t%2 == 0)
                    x = N+i;
                else
                    x = N-1-i;
                int y = N-1-k;

                if (t%2 == 1)
                   swap (x, y);

                // cout << z << " " << y << " " << x << endl;
                assert (z >= 0 && z < 3);
                assert (y >= 0 && y < N);
                assert (x >= 0 && x < 2*N);
                board [z][y][x] = box;
                box->setToolTip (name (x, y, z));
            }
        }
    }

    for (int x = 0; x < 2*N; x++)
    {
        addPawn (x, 1, 0, QColor ("blue"));
        addPawn (x, 1, 1, QColor ("red"));
        addPawn (x, 1, 2, QColor ("gray"));
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}


