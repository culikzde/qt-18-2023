#include "example.h"

#include <QApplication>

#include <Qt3DCore/QEntity>
#include <Qt3DRender/QCamera>
#include <Qt3DRender/QCameraLens>
#include <Qt3DCore/QTransform>
#include <Qt3DCore/QAspectEngine>

#include <Qt3DInput/QInputAspect>

#include <Qt3DRender/QRenderAspect>
#include <Qt3DRender/QGeometryRenderer>
#include <Qt3DExtras/QForwardRenderer>
#include <Qt3DExtras/QPhongMaterial>
#include <Qt3DExtras/QSphereMesh>

#include <Qt3DExtras/QTorusMesh>
#include <Qt3DExtras/QCuboidMesh>
#include <Qt3DExtras/QSphereMesh>
#include <Qt3DExtras/QConeMesh>
#include <Qt3DExtras/QCylinderMesh>

#include <Qt3DExtras/Qt3DWindow>
#include <Qt3DExtras/QOrbitCameraController>

#include <vector>
#include <iostream>
using namespace std;

// const int N = 3; // pocet rozmeru
const int P = 4; // pocet policek v jedne rade

vector <vector <int>> a (P, vector (P, 0));

void init ()
{
    for (int x = 0; x < P; x++)
        for (int y = 0; y < P; y++)
            a[x][y] = -1;
}

int cnt = 0;

void print ()
{
    cnt ++;
    cout << endl;
    cout << "RESULT " << cnt << endl;
    cout << endl;
    for (int x = 0; x < P; x++)
    {
        for (int y = 0; y < P; y++)
        {
            int v = a[x][y];
            char c = '0' + v;
            if (v < 0) c = '.';
            cout << c << " ";
        }
        cout << endl;
    }
    cout << endl;
}

void play (int x, int y, int level =1)
{
   for (int i = 1; i <= level; i ++) cout << "   ";
   cout << "play (" << x << ", " << y << ", level = " << level << ")" << endl;

   for (int z = 0; z < P; z++)
   {
       bool ok = true;
       for (int v = 1; v < P && ok; v++)
       {
           for (int dx = -1; dx <= 1 && ok; dx++)
               for (int dy = -1; dy <= 1 && ok; dy++)
                   for (int dz = -1; dz <= 1 && ok; dz++)
                       // if (dx != 0 || dy != 0 || dz != 0)
                       if (abs (dx) + abs (dy) + abs (dz) == 1)
                       {
                           int nx = x + v*dx;
                           int ny = y + v*dy;
                           int nz = x + v*dz;

                           if (nx >= 0 && nx < P && ny >= 0 && ny < P && nz >= 0 && nz < P)
                           {
                              if (a[nx][ny] == nz)
                                  ok = false;
                           }

                       }

       }
       if (ok)
       {
           a[x][y] = z;
           if (x < P-1) play (x+1, y, level+1);
           else if (y < P-1) play (0, y+1, level+1);
           else print ();
           a[x][y] = -1;
       }
   }
}

using namespace Qt3DCore;
using namespace Qt3DExtras;

Qt3DCore::QEntity *createScene()
{
    Qt3DCore::QEntity *rootEntity = new Qt3DCore::QEntity;

    QPhongMaterial *whiteMaterial = new QPhongMaterial (rootEntity);
    whiteMaterial->setAmbient (QColor ("white"));

    Qt3DExtras::QPhongMaterial *yellowMaterial = new Qt3DExtras::QPhongMaterial (rootEntity);
    yellowMaterial->setAmbient (QColor ("yellow"));

    const float W = 10; // sirka a vyska policka sachovnice
    const float H = 0.1; // vyska policka

    for (int z = 0; z < P; z++)
        for (int x = 0; x < P; x++)
            for (int y = 0; y < P; y++)
            {
                QEntity * boxEntity = new QEntity (rootEntity);
                QCuboidMesh * boxMesh = new QCuboidMesh;
                boxMesh->setXExtent (W);
                boxMesh->setZExtent (W);
                boxMesh->setYExtent (H);
                Qt3DCore::QTransform * boxTransform = new Qt3DCore::QTransform;
                boxTransform->setTranslation (QVector3D (x*W, z*W, y*W));
                boxEntity->addComponent (boxMesh);
                boxEntity->addComponent (boxTransform);
                if ((x+y+z)%2 == 0)
                    boxEntity->addComponent (whiteMaterial);
                else
                    boxEntity->addComponent (yellowMaterial);
            }

    /*
    Qt3DCore::QEntity *rootEntity = new Qt3DCore::QEntity;

    Qt3DExtras::QPhongMaterial *material = new Qt3DExtras::QPhongMaterial (rootEntity);
    material->setAmbient (QColor ("blue"));

    Qt3DCore::QEntity *torusEntity = new Qt3DCore::QEntity(rootEntity);
    Qt3DExtras::QTorusMesh *torusMesh = new Qt3DExtras::QTorusMesh;
    torusMesh->setRadius(5);
    torusMesh->setMinorRadius(1);
    torusMesh->setRings(100);
    torusMesh->setSlices(20);

    Qt3DCore::QTransform *torusTransform = new Qt3DCore::QTransform;
    torusTransform->setScale3D(QVector3D(1.5, 1, 0.5));
    torusTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), 45.0f));

    torusEntity->addComponent(torusMesh);
    torusEntity->addComponent(torusTransform);
    torusEntity->addComponent(material);

    Qt3DCore::QEntity *sphereEntity = new Qt3DCore::QEntity(rootEntity);
    Qt3DExtras::QSphereMesh *sphereMesh = new Qt3DExtras::QSphereMesh;
    sphereMesh->setRadius(3);
    sphereMesh->setGenerateTangents(true);

    sphereEntity->addComponent(sphereMesh);
    sphereEntity->addComponent(material);
    */

    return rootEntity;
}

int main (int argc, char* argv[])
{
    init ();
    play (0,0);
    cout << "RESULTS " << cnt << endl;

#if 0
    QApplication app (argc, argv);
    Qt3DExtras::Qt3DWindow view;

    Qt3DCore::QEntity * scene = createScene();

    Qt3DRender::QCamera * camera = view.camera();
    camera->lens()->setPerspectiveProjection (45.0f, 16.0f/9.0f, 0.1f, 1000.0f);
    camera->setPosition (QVector3D(40, 40, 200));
    camera->setViewCenter (QVector3D(0, 0, 0));

    Qt3DExtras::QOrbitCameraController * camController = new Qt3DExtras::QOrbitCameraController (scene);
    camController->setLinearSpeed( 50.0f );
    camController->setLookSpeed( 180.0f );
    camController->setCamera(camera);

    view.setRootEntity(scene);
    view.show();

    return app.exec();
#endif
}
