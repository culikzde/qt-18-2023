﻿#ifndef PANEL_H
#define PANEL_H

#include <QGraphicsProxyWidget>

#include <QTreeWidget>
#include <QTableView>
#include <QTableWidget>
#include <QTextEdit>
#include <QSplitter>
#include <QPushButton>

#include <QToolBar>
#include <QStatusBar>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QMenu>

#include <QXmlStreamReader>
#include <QXmlStreamWriter>

#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>

#include <QMetaObject>
#include <QMetaMethod>

#ifdef JS
   #include "js.h"
#endif

#ifdef SQL
    class QSqlError;
#endif

#ifdef SOCKET
   class QTcpServer;
   class QTcpSocket;
#endif

#ifdef SVG_PANEL
   class QSvgWidget;
   class QNetworkReply;
#endif

#ifdef WEBENGINE
    class QWebEngineView;
#endif

#ifdef WEBKIT
    class QWebView;
#endif

/* ---------------------------------------------------------------------- */

class Draw;
class MyTreeItem;

class Panel : public QGraphicsProxyWidget
{
Q_OBJECT
public:
    Panel ();

public:
    virtual QString typeName () { return "panel"; }

    virtual void contextMenu (QMenu * menu);
    virtual void setupTreeItem (MyTreeItem * node);
    virtual void displayProperties (Draw * win);
    virtual void storeProperty (QString name, QVariant value);

    virtual void readItem (QJsonObject & obj);
    virtual void writeItem (QJsonObject & obj);

    virtual void readXmlItem (QXmlStreamReader & reader);
    virtual void writeXmlItem (QXmlStreamWriter & writer);

protected:
    virtual QStringList attrNames () { return  QStringList (); }
    virtual QVariant getAttr (QString name);
    virtual void setAttr (QString name, QVariant value);

    Q_INVOKABLE void select () { setSelected (true); }
};

/* ---------------------------------------------------------------------- */

class TextPanel : public Panel
{
Q_OBJECT
public:
    TextPanel ();
    QString typeName () override { return "text"; }
private:
    QTextEdit * edit;

private:
    Q_PROPERTY (QString text READ text WRITE setText)
    Q_PROPERTY (QString title READ title WRITE setTitle NOTIFY titleChanged)
    Q_PROPERTY (int number MEMBER number)

private:
    int number = 7;

public:
    QString text () { return edit->toPlainText(); }
    void setText (QString value) { edit->setPlainText (value); }

public:
    QString title () { return titleVar; }

    void setTitle (QString value)
    {
       titleVar = value;
       setWindowTitle (value);
       emit titleChanged (value);
    }

Q_SIGNALS :
    void titleChanged (QString value);

private:
    QString titleVar;

protected:
     QStringList attrNames () override { return  QStringList () << "text" << "title" << "number"; }
};


/* ---------------------------------------------------------------------- */

typedef QList <QStringList> StringTable;

typedef QList <QVariant> DataLine;

struct DataCollection
{
    QString title;
    QStringList columns;
    QList <DataLine> lines;
};

void run ();
// void testConnection ();

class DataPanel : public Panel
{
Q_OBJECT
public:
    DataPanel ();
    QString typeName () override { return "data"; }
public:
    DataCollection data;
public Q_SLOTS:
    DataCollection sendData ();
};

/* ---------------------------------------------------------------------- */

class TablePanel : public Panel
{
Q_OBJECT

public:
    DataCollection data;
public Q_SLOTS:
    DataCollection sendData () { return data; }
    void receiveData (DataCollection p_data) { data = p_data; updateTable (); }

public:
    TablePanel ();
    QString typeName () override { return "table"; }

private:
    void updateTable ();

private:
    QTableWidget * table = nullptr;
};

/* ---------------------------------------------------------------------- */

class ClipboardPanel : public Panel
{
public:
    ClipboardPanel ();
    QString typeName () override { return "clip"; }
private:
    QTextEdit * edit;
    void readJson ();
    void writeJson ();
    void readXml ();
    void writeXml ();
};

/* ---------------------------------------------------------------------- */

#ifdef SQL

class SqlPanel : public Panel
{
Q_OBJECT
public:
   SqlPanel ();
   QString typeName () override { return "sql"; }
private:
   QTreeWidget * tree;
   QTableView * table;
   QTextEdit * info;

private:
   void message (QSqlError err);
   void example ();
};

#endif

/* ---------------------------------------------------------------------- */

#ifdef DBUS

class DbusPanel : public Panel
{
    Q_OBJECT
    Q_CLASSINFO ("D-Bus Interface", "org.example.ReceiverInterface")

public:
   DbusPanel ();
   ~ DbusPanel ();
   QString typeName () override { return "dbus"; }

public slots:
    Q_SCRIPTABLE void hello (QString s);

private:
   QTextEdit * info;
};

#endif

/* ---------------------------------------------------------------------- */

#ifdef DBUS

class DbusSendPanel : public Panel
{
public:
   DbusSendPanel ();
   QString typeName () override { return "dbus_send"; }

private:
   QLineEdit * lineEdit;
   QPushButton * sendButton;

   void send ();
};

#endif

/* ---------------------------------------------------------------------- */

#ifdef SVG_PANEL

class SvgPanel : public Panel
{
Q_OBJECT
public:
   SvgPanel ();
   QString typeName () override { return "svg"; }
private :
   QSvgWidget * view;
public slots:
    void replyFinished (QNetworkReply * reply);
};

#endif

/* ---------------------------------------------------------------------- */

#ifdef SOCKET

class SocketPanel : public Panel
{
Q_OBJECT
   public:
      explicit SocketPanel (int port_param = 1234);
      ~ SocketPanel ();
      QString typeName () override { return "socket"; }

      void run();

   private:
      int port;
      QTcpServer * server;

      QTextEdit * info ;

      void print (QString s);
      void connection ();

   private:
      DataCollection data;
   public Q_SLOTS:
      void receiveData (DataCollection p_data) { data = p_data; }
};

#endif

/* ---------------------------------------------------------------------- */

#ifdef JS

class JsWindow : public QWidget
{
Q_OBJECT
public:
    JsWindow (QWidget * parent = nullptr);
    void run ();
    void debug ();
private:
    JsEdit * edit;
};

class JsPanel : public Panel
{
Q_OBJECT
public:
   JsPanel ();
   QString typeName () override { return "js"; }
private:
   JsWindow * window;
   Q_INVOKABLE void debug () { window->debug (); }
   Q_INVOKABLE void run () { window->run (); }
};

#endif

/* ---------------------------------------------------------------------- */

#ifdef WEBENGINE

class WebEngineWindow : public QWidget
{
public:
    WebEngineWindow (QWidget * parent = nullptr);
    void load (QUrl url);

private:
    QVBoxLayout * layout;
    QToolBar * toolBar;
    QLineEdit * locationEdit;
    QWebEngineView * view;
    QStatusBar * statusBar;

   void adjustLocation ();
   void changeLocation ();
   void setProgress (int p);
};

class WebPanel : public Panel
{
Q_OBJECT
public:
    WebPanel ();
    QString typeName () override { return "web"; }
    void load (QUrl url);

private:
    WebEngineWindow * window;
};

#endif

/* ---------------------------------------------------------------------- */

#ifdef WEBKIT

class WebKitWindow : public QWidget
{
public:
    WebKitWindow (QWidget * parent = nullptr);
    void load (QUrl url);

private:
    QVBoxLayout * layout;
    QToolBar * toolBar;
    QLineEdit * locationEdit;
    QWebView * view;
    QStatusBar * statusBar;

   void adjustLocation ();
   void changeLocation ();
   void setProgress (int p);
};

class WebKitPanel : public Panel
{
Q_OBJECT
public:
    WebKitPanel ();
    QString typeName () override { return "webkit"; }
    void load (QUrl url);

private:
    WebKitWindow * window;
};

#endif

/* ---------------------------------------------------------------------- */

#ifdef CHART

class ChartPanel : public Panel
{
Q_OBJECT
public:
    ChartPanel ();
    QString typeName () override { return "chart"; }
public Q_SLOTS:
    void receiveData (DataCollection data);
};

class PiePanel : public Panel
{
Q_OBJECT
public:
    PiePanel ();
    QString typeName () override { return "pie"; }
public Q_SLOTS:
    void receiveData (DataCollection data);
};

#endif

/* ---------------------------------------------------------------------- */

#ifdef VISUALIZATION

QWidget * createVisualizationWindow (QWidget * parent = nullptr);

class VisualizationPanel : public Panel
{
public:
    VisualizationPanel ();
    QString typeName () override { return "visualization"; }
private:
    QWidget * window;
};

#endif

/* ---------------------------------------------------------------------- */

#ifdef QT3D

QWidget * createQt3DWindow (QWidget * parent = nullptr);

class Qt3DPanel : public Panel
{
public:
    Qt3DPanel ();
    QString typeName () override { return "qt3d"; }
private:
    QWidget * window;
};

#endif

/* ---------------------------------------------------------------------- */

QStringList componentTools ();
QGraphicsItem * createComponent (QString tool);

#endif // PANEL_H
