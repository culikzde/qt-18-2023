#ifndef WINDOW_H
#define WINDOW_H

#include <QMainWindow>
#include <QTreeWidget>
#include <QDir>

QT_BEGIN_NAMESPACE
namespace Ui { class Window; }
QT_END_NAMESPACE

class Window : public QMainWindow
{
    Q_OBJECT

public:
    Window(QWidget *parent = nullptr);
    ~Window();

private:
    void displayDir (QTreeWidgetItem * branch, QDir dir);
public:
    void displayDirectory (QString path);

private slots:
    void on_pushButton_clicked();

private:
    Ui::Window *ui;
};
#endif // WINDOW_H
