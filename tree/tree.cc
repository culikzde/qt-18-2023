#include "tree.h"
#include "ui_tree.h"
#include <QTreeWidgetItem>
#include <QDir>
#include <QFileInfo>
#include <QDateTime>
#include <QApplication>

Window::Window(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Window)
{
    ui->setupUi(this);
    displayDirectory ("..");
}

Window::~Window()
{
    delete ui;
}

void Window::displayDir (QTreeWidgetItem * branch, QDir dir)
{
    QFileInfoList list = dir.entryInfoList (QDir::AllEntries | QDir::NoDotAndDotDot);
    for (QFileInfo info : list)
    {
        QTreeWidgetItem * node = new QTreeWidgetItem;
        node->setText (0, info.fileName());
        node->setToolTip (0, info.filePath());
        if (info.isDir())
            node->setForeground (0, QColor (255, 0, 0));
        else
            node->setForeground (0, QColor (0, 0, 255));
        if (info.isDir())
           displayDir (node, QDir (info.filePath()));
        else
        {
            qint64 n = info.size();
            QString s = QString::number (n);

            while (s.length () < 9) s = " " + s;

            QString t = "";
            while (s.length () > 3)
            {
                t = " " + s.right (3) + t;
                s = s.left (s.length()-3);
            }
            t = s + t;

            node->setText (1, t);
            node->setText (2, info.lastModified().toString ("yyyy-MM-dd hh:mm"));
        }
        branch->addChild (node);
    }
}

void Window::displayDirectory (QString path)
{
    ui->treeWidget->setHeaderLabels (QStringList () << "name" << "size" << "date" << "");

    QDir dir0 (path);
    QDir dir (dir0.absolutePath());

    QTreeWidgetItem * top = new QTreeWidgetItem;
    top->setText (0, dir.dirName());
    top->setToolTip (0, dir.absolutePath ());
    top->setForeground (0, QColor ("red"));

    displayDir (top, dir);

    ui->treeWidget->addTopLevelItem (top);
    ui->treeWidget->expandItem (top);
}

void Window::on_pushButton_clicked()
{
}


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Window w;
    w.show();
    return a.exec();
}
